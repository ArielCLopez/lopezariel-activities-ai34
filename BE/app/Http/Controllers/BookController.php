<?php

namespace App\Http\Controllers;
use App\Models\Book;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Http\Requests\BookRequest;
class BookController extends Controller

{
    public function index()
    {
    return response()->json(Book::with(['category:id,category'])->get());
    }
    
    public function store(BookRequest $request)
    {
        return response()->json(Book::create($request->all()));
    }

    public function show($id)
    {
        try{
    $book = Book::with(['category:id,category'])->where('id', $id)->firstOrFail();
         return response()->json($book);
        }catch (ModelNotFoundExecption $execption){
        return response()->json(['message'=>'Book not Found']);
        }
    }

    public function update(BookRequest $request, $id)
    {
        $book = Book::with(['category:id,category'])->where('id', $id)->firstOrFail();
        $book->update($request->all());
        $updated = Book::with(['category:id,category'])->find($book->id);
        return response()->json(['message' => 'Book updated successfully!', 'book' => $updated]);
    }

    public function destroy($id)
    {
        try{
        $book = Book::where('id', $id)->firstOrFail();
        $book->delete();
        return response()->json(['message' => 'Book deleted successfully!']);
        }catch (ModelNotFoundExecption $execption){
        return response()->json(['message'=>'Book not Found']);
        
        }
    }

}