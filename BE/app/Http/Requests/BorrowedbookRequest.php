<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BorrowedbookRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
        $book = Book::find($request->id);
        if (empty($book)){
            $copies = $request->copies;
        
         } else{
            $copies = $request->copies;  
        }
        return [
                'book_id' => ['bail','required','exist:book,id'],
                'copies' => ['bail','required',"lte:{$copies}",'integer','gt:0'],
                'patron_id' => ['exist:patron,id'],
        ];
    }
}
