<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ReturnbookRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
        $borowed_book = borrowedbooks::where('book_id'->$request->book_id)->where('patron_id'->$request->patron_id)->firstOrFail();
        if (empty($book)){
            $copies = $request->copies;
            
        }else{
                $copies = $request->copies;
        }

        return [
                 'book_id' => ['bail','required','exist:book,id'],
                'copies' => ['bail','required',"lte:{$copies}",'integer','gt:0'],
                'patron_id' => ['exist:patron,id'],
        ];
    }

    public function message(){
        return[
            'book_id.required' => "Book is is required",
            'book_id.exists' => "Book id must exist in the borrowed Books",
            'book_id.integer' =>"Copies must be an Integer",
            'patron_id.exists' =>"Patron id must exist in patron table",
        ];
    }
}
