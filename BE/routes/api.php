<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\BookController;
use App\Http\Controllers\PatronController;
use App\Http\Controllers\BorrowedBookController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\ReturnedBookController;


Route::resource('patrons', PatronController::class)->only(['index', 'store', 'show', 'update', 'destroy']);
Route::resource('books', BookController::class)->only(['index', 'store', 'show', 'update', 'destroy']);

Route::resource('/borrowedbook', BorrowedBookController::class);
Route::resource('/returnedbook', ReturnedBookController::class);

Route::get('/categories', [CategoryController::class, 'index']);
