let labels = ['January', 'Febuary', 'March', 'April','June'];
let data2 = [3,5, 10, 15, 20, 25];
let colors2 = ['#9b12bd', '#2530cc', '#1f1a3d', '#f09e08','#4866EC'];

let myChart2 = document.getElementById("myChart2").getContext('2d');

let chart2 = new Chart(myChart2, {
    type: 'bar',
    data: {
        labels: labels,
        datasets: [ {
            data: data2,
            backgroundColor: colors2
        }]
    },
    options: {
        title: {
            text: "Number of return and borrow books.)",
            display: true
        },
        legend: {
          display: false
        }
    }
});


let labels4 = ['Java', 'Phyton', '.NET','C++'];
let data4 = [5, 10, 15, 20,25];
let colors4 = ['#9b12bd', '#2530cc', '#1f1a3d', '#f09e08','#4866EC'];

let myChart4 = document.getElementById("myChart4").getContext('2d');

let chart4 = new Chart(myChart4, {
    type: 'pie',
    data: {
        labels: labels4,
        datasets: [ {
            data: data4,
            backgroundColor: colors4
        }]
    },
    options: {
        title: {
            text: "Category uses",
            display: true
        }
    }
});