import Vue from 'vue'
import { BootstrapVue, IconsPlugin} from 'bootstrap-vue'
import router from './router'
import App from './views/App.vue'
import Toast from "vue-toastification";
import store from './store'

Vue.use(BootstrapVue)
Vue.use(IconsPlugin)
Vue.config.productionTip = false

import 'bootstrap' 
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import "vue-toastification/dist/index.css";

Vue.use(Toast, {
  transition: "Vue-Toastification__fade",
  maxToasts: 4,
  newesOnTop: true
}

);

new Vue({
  router,
  store,
  render: function (h) { return h(App) }
}).$mount('#app')



